#pragma once
#include "GameObject.h"

class Level
{

	

public:
	Level();
	~Level();
	struct ObjectData{
		const char* name;
		XMFLOAT3 position;
		XMFLOAT3 rotation;
		XMFLOAT3 scale;
		float mass;
		ObjectData(const char* name, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale, float mass) : name(name), position(position), rotation(rotation), scale(scale), mass(mass)
		{
		}
	};
	std::vector<ObjectData*> objects;

};

