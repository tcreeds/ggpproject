#include "GGPMath.h"


GGPMath::GGPMath()
{
}


GGPMath::~GGPMath()
{
}

float GGPMath::dot(XMFLOAT3 a, XMFLOAT3 b){
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

float GGPMath::dot(btVector3* a, btVector3* b){
	float result = a->x() * b->x() + a->y() * b->y() + a->z() * b->z();
	return result;
}