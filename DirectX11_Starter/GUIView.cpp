#include "GUIView.h"


GUIView::GUIView(std::string name)
{
	this->name = name;
}


GUIView::~GUIView()
{
}

void GUIView::AddElement(GUIElement element){
	this->elements.push_back(element);
	if (elements.size() == 1)
		selectedElement = &elements[0];
}

void GUIView::Select(GUIElement* element)
{
	selectedElement = element;
	element->selected = true;
}

void GUIView::Deselect(GUIElement* element){
	element->selected = false;
	if (selectedElement == element)
		selectedElement = 0;
}

void GUIView::Press(GUIElement* element){
	element->pressed = true;
}

void GUIView::Keyboard(Keypress key){
	
}

GUIElement* GUIView::GetElementByName(std::string name)
{
	for (int i = 0; i < elements.size(); i++){
		if (elements[i].name == name)
			return &elements[i];
	}
	return 0;
}
