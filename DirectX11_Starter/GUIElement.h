#pragma once
#include "Texture.h"

class GUIElement
{
public:
	GUIElement(float width, float height, float x, float y, Texture* defaultTexture, Texture* selectedTexture, std::string name);
	GUIElement(float width, float height, float x, float y, Texture* defaultTexture, Texture* selectedTexture, Texture* pressedTexture, std::string name);
	GUIElement(float width, float height, float x, float y, Texture* defaultTexture, Texture* selectedTexture, std::string name, std::string action);
	GUIElement(float width, float height, float x, float y, Texture* defaultTexture, Texture* selectedTexture, Texture* pressedTexture, std::string name, std::string action);
	~GUIElement();
	std::string name;
	std::string action;
	float width;
	float height;
	float x;
	float y;
	bool selected;
	bool pressed;
	Texture* defaultTexture;
	Texture* selectedTexture;
	Texture* pressedTexture;
};

