#pragma once
#include "GameObject.h"
#include "btBulletDynamicsCommon.h"

class PhysicsManager
{
public:
	PhysicsManager();
	~PhysicsManager();
	void Update(float dt);
	btRigidBody* CreateBody(XMFLOAT3 position, const char* type, XMFLOAT3 halfWidths, XMFLOAT3 rotation, XMFLOAT3 scale, float mass);
	btRigidBody* CreateBody(XMFLOAT3 dimensions, XMFLOAT3 rotation, XMFLOAT3 scale, float mass);
	void RegisterBody(GameObject* go);
	void CreateAndRegisterBody(XMFLOAT3 position, XMFLOAT3 dimensions, XMFLOAT3 halfWidths, XMFLOAT3 rotation, XMFLOAT3 scale, float mass);
	void MakeGround();
	btDiscreteDynamicsWorld* GetWorld();
	void SyncWithGraphic(GameObject* object);

private:
	btBroadphaseInterface*                  broadphase;
	btDefaultCollisionConfiguration*        collisionConfiguration;
	btCollisionDispatcher*                  dispatcher;
	btSequentialImpulseConstraintSolver*    solver;
	btDiscreteDynamicsWorld*                world;
};

