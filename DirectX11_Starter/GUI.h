#pragma once
#include "GUIView.h"
#include "GraphicsManager.h"

class GUI
{
public:
	GUI(GraphicsManager* graphicsManager);
	~GUI();
	void MouseMove(float x, float y);
	std::string MouseDown();
	void MouseUp();
	GUIView* currentView;
	std::vector<GUIView> views;

};

