#pragma once
//#include "btBulletDynamicsCommon.h"
#include "GameObject.h"

class Player : public GameObject
{

public:
	Player();
	Player(Mesh* mesh, Material* material, Texture* texture, btRigidBody* body, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale);
	Player(Mesh* mesh);
	~Player();
	int jumps;
	int maxJumps;
	float jumpLockTime;
	bool canDie;
	void Update(float dt);
	void TakeInput();
	void Jump();
	enum Direction{ LEFT, RIGHT, FORWARD, BACK, UP, DOWN };
	void Turn(Direction dir);
	void Move(Direction dir);
	void ResetJump();

private:
	float jumpForce;
	float speed;
	float turnSpeed;

};

