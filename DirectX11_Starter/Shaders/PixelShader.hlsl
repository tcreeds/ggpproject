

struct DirectionalLight
{
	float4 AmbientColor;
	float4 DiffuseColor;
	float3 Direction;
	float padding;
};

struct PointLight
{
	float4 Position;
	float4 Color;
	float Range;
	float3 padding;
};

struct SpotLight
{
	float4 Color;
	float4 Position;
	float4 Direction;
	float Intensity;
	float3 padding;
};

cbuffer perModel : register(b0)
{
	DirectionalLight dirLight;
	PointLight pointLights[8];
	SpotLight spotLights[8];
	uint numPointLights;
	uint numSpotLights;
};

Texture2D diffuseTexture : register(t0);
Texture2D shadowMap		 : register(t1);
SamplerState basicSampler : register(s0);

SamplerState pointSampler 
{
	Filter = min_mag_mip_point;
	AddressU = MIRROR;
	AddressV = MIRROR;
};

// Defines the input to this pixel shader
// - Should match the output of our corresponding vertex shader
struct VertexToPixel
{
	float4 position		: SV_POSITION;
	float3 normal		: NORMAL;
	float2 uv			: TEXCOORD;
	float3 worldPos		: TEXCOORD1;
	float4 screenPosLight	: TEXCOORD2;
};

// Entry point for this pixel shader
float4 main(VertexToPixel input) : SV_TARGET
{
	float bias = .00004f;

	//return float4(depth, 0, 0, 1);
	input.normal = normalize(input.normal);
	float4 surfaceColor = diffuseTexture.Sample(basicSampler, input.uv);

	float3 normlight = normalize(-(dirLight.Direction));

	float3 newnormlight = normlight*surfaceColor;

	float3 lightam = saturate(dot(input.normal, newnormlight));
	float3 newlightam = lightam*surfaceColor;

	float3 li1 = ((dirLight.DiffuseColor * newlightam) + dirLight.AmbientColor);

	input.screenPosLight.xyz /= input.screenPosLight.w;
	if (input.screenPosLight.x < -1.0f || input.screenPosLight.x > 1.0f ||
		input.screenPosLight.y < -1.0f || input.screenPosLight.y > 1.0f ||
		input.screenPosLight.z < 0.0f || input.screenPosLight.z > 1.0f)
		return float4(li1 * 0.5, 1);



	input.screenPosLight.x = input.screenPosLight.x / 2 + 0.5;
	input.screenPosLight.y = input.screenPosLight.y / -2 + 0.5;

	//see if the depth in the shadowmap is smaller than the depth from POV of light, if so it is in the shadow and isn't lit up
	float depth = shadowMap.Sample(basicSampler, input.screenPosLight.xy).r;
	//float3 li2 = ((l2.DiffuseColor * newlightam2) + l.AmbientColor);

	[unroll]
	for (int i = 0; i < numPointLights; i++){
		float3 dir = pointLights[i].Position - input.worldPos;
			float amtLight = dot(input.normal, normalize(dir));
		float dist = length(dir) / pointLights[i].Range;

		//finalColor = saturate(abs(normalize(dir)));
		//finalColor += pointLights[i].Color * amtLight * (1.0f - saturate(dist / pointLights[i].Range));
		li1 = li1 + pointLights[i].Color * (1.0f - saturate(dist));
		//finalColor *= pointLights[i].Range;
	}

	if (depth < (input.screenPosLight.z - bias))
		return float4(li1 * 0.5, 1);

	else
	{
		return float4(li1, 1);
	}
}

