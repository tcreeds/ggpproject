#include "PhysicsManager.h"

PhysicsManager::PhysicsManager()
{
	// Set up physiscs world
	broadphase = new btDbvtBroadphase();
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);
	solver = new btSequentialImpulseConstraintSolver();
	world = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	world->setGravity(btVector3(0, -30, 0));
}


PhysicsManager::~PhysicsManager()
{
	delete world;
	delete solver;
	delete dispatcher;
	delete collisionConfiguration;
	delete broadphase;
}

btRigidBody* PhysicsManager::CreateBody(XMFLOAT3 position, const char* type, XMFLOAT3 halfWidths, XMFLOAT3 rotation, XMFLOAT3 scale, float mass){

	if (strcmp(type, "Box") == 0){
		btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
		btRigidBody* body = new btRigidBody(mass, state, new btBoxShape(btVector3(halfWidths.x * scale.x, halfWidths.y * scale.y, halfWidths.z * scale.z)));
		//btVector3 inertia;
		//body->getCollisionShape()->calculateLocalInertia(mass, inertia);
		btTransform transform = body->getCenterOfMassTransform();
		transform.setOrigin(btVector3(position.x, position.y, position.z));
		body->setCenterOfMassTransform(transform);
		body->setMassProps(mass, btVector3(0, 0, 0));
		return body;
	}
	else if (strcmp(type, "Sphere") == 0){
		btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
		btRigidBody* body = new btRigidBody(mass, state, new btSphereShape(halfWidths.x));
		btTransform transform = body->getCenterOfMassTransform();
		transform.setOrigin(btVector3(position.x, position.y, position.z));
		body->setCenterOfMassTransform(transform);
		body->setMassProps(mass, btVector3(0, 0, 0));
		return body;
	}
	else if (strcmp(type, "Capsule") == 0){
		btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
		btRigidBody* body = new btRigidBody(mass, state, new btCapsuleShape(halfWidths.x * scale.x, halfWidths.y * scale.y));
		//btVector3 inertia;
		//body->getCollisionShape()->calculateLocalInertia(mass, inertia);
		btTransform transform = body->getCenterOfMassTransform();
		transform.setOrigin(btVector3(position.x, position.y, position.z));
		body->setCenterOfMassTransform(transform);
		body->setMassProps(mass, btVector3(0, 0, 0));
		return body;
	}
	else if (strcmp(type, "Trigger") == 0){
		
		btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
		btRigidBody* body = new btRigidBody(mass, state, new btCapsuleShape(halfWidths.x * scale.x, halfWidths.y * scale.y));
		body->setCollisionFlags(body->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
		btTransform transform = body->getCenterOfMassTransform();
		transform.setOrigin(btVector3(position.x, position.y, position.z));
		body->setCenterOfMassTransform(transform);
		return body;
	}
	return 0;
}

btRigidBody* PhysicsManager::CreateBody(XMFLOAT3 halfWidths, XMFLOAT3 rotation, XMFLOAT3 scale, float mass){

	btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	btRigidBody* body = new btRigidBody(mass, state, new btBoxShape(btVector3(halfWidths.x * scale.x, halfWidths.y * scale.y, halfWidths.z * scale.z)));
	//btVector3 inertia;
	//body->getCollisionShape()->calculateLocalInertia(mass, inertia);
	body->setMassProps(mass, btVector3(0, 0, 0));
	return body;
}

void PhysicsManager::RegisterBody(GameObject* object)
{
	// Set the rigidbody to the object's position
	btQuaternion bulletQuat = btQuaternion(object->transform->GetRotation()->x, object->transform->GetRotation()->y, object->transform->GetRotation()->z);
	btTransform bulletTransform = btTransform(bulletQuat, btVector3(object->transform->GetPosition()->x, object->transform->GetPosition()->y, object->transform->GetPosition()->z));
	object->body->setWorldTransform(bulletTransform);

	// Add the rigidbody to the world
	world->addRigidBody(object->body);

	// Activate the rigidbody and make it so it doesn't turn off its physics after a certain period
	object->body->activate();
	object->body->setActivationState(DISABLE_DEACTIVATION);
}

void PhysicsManager::CreateAndRegisterBody(XMFLOAT3 position, XMFLOAT3 dimensions, XMFLOAT3 halfWidths, XMFLOAT3 rotation, XMFLOAT3 scale, float mass){

	btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	btRigidBody* body = new btRigidBody(mass, state, new btBoxShape(btVector3(halfWidths.x * scale.x, halfWidths.y * scale.y, halfWidths.z * scale.z)));
	//btVector3 inertia;
	//body->getCollisionShape()->calculateLocalInertia(mass, inertia);
	body->setMassProps(mass, btVector3(0, 0, 0));

	btQuaternion bulletQuat = btQuaternion(rotation.x,rotation.y,rotation.z);
	btTransform bulletTransform = btTransform(bulletQuat, btVector3(position.x, position.y, position.z));
	body->setWorldTransform(bulletTransform);

	world->addRigidBody(body);

	body->activate();
	body->setActivationState(DISABLE_DEACTIVATION);
}

void PhysicsManager::MakeGround()
{
	// Makes a plane collision box to serve as the ground for now
	btCollisionShape* ground = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	btDefaultMotionState* groundState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0, groundState, ground, btVector3(0, 0, 0));
	btRigidBody* groundRigidBody = new btRigidBody(groundRigidBodyCI);
	world->addRigidBody(groundRigidBody);
}

void PhysicsManager::Update(float dt)
{
	// Step the physics world
	world->stepSimulation(dt, 10);
}

btDiscreteDynamicsWorld* PhysicsManager::GetWorld()
{
	return world;
}

void PhysicsManager::SyncWithGraphic(GameObject* object)
{
	btTransform transform = object->body->getInterpolationWorldTransform();
	// Set the graphic object's position to the rigidbody's origin
	btQuaternion quat = transform.getRotation();
	XMFLOAT3 rot;
	float x = quat.x();
	float y = quat.y();
	float z = quat.z();
	float w = quat.w();
	rot.x = atan2(2 * (x * y + z * w), 1 - 2 * (y * y + z * z));
	rot.y = asin(2 * (x * z - w * y)) + 1.57f;
	rot.z = atan2(2 * (x * w + y * z), 1 - 2 * (z * z + w * w));
	object->transform->SetPosition(XMFLOAT3(transform.getOrigin().x(), transform.getOrigin().y(), transform.getOrigin().z()));
	//object->transform->SetRotation(rot);
}
