#pragma once

#include "DirectXGame.h"
#include <vector>
#include "Transform.h"
#include "Material.h"

using namespace DirectX;

class Mesh
{
public:
	Mesh();
	Mesh(const char* name);
	~Mesh();
	void SetData(std::vector<Vertex> vertices, int numVertices, std::vector<UINT> indices, int numIndices);
	void SetBuffers(ID3D11Buffer* vb, ID3D11Buffer* ib);
	ID3D11Buffer* const* getVertexBuffer();
	ID3D11Buffer* getIndexBuffer();
	std::vector<Vertex> getVertices();
	std::vector<UINT> getIndices();
	const char* GetName();
	int getIndexCount();
	bool IsEnabled();
	void Enable();
	void Disable();

private:
	const char* name;
	ID3D11Buffer* vertexBuffer;
	ID3D11Buffer* indexBuffer;
	std::vector<Vertex> vertices;
	int numVertices;
	std::vector<UINT> indices;
	int numIndices;
	bool enabled;
};

