#include "GameObject.h"

GameObject::GameObject()
{
	this->transform = new Transform();

	/*// Make a default rigidbody
	btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	body = new btRigidBody(1, state, new btBoxShape(btVector3(1, 1, 1)), btVector3(transform->GetRotation()->x, transform->GetRotation()->y, transform->GetRotation()->z));
	*/
}

GameObject::GameObject(Mesh* mes, Material* mat, Texture* texture, btRigidBody* bod, XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale) 
	: mMesh(mes), mMaterial(mat), texture(texture), body(bod)
{
	transform = new Transform();
	transform->SetPosition(pos);
	transform->SetRotation(rot);
	transform->SetScale(scale);
	enabled = true;
}

GameObject::GameObject(bool addCollider)
{
	this->transform = new Transform();

	// Make a default rigidbody
	if (addCollider){
		btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
		body = new btRigidBody(1, state, new btBoxShape(btVector3(1, 1, 1)), btVector3(transform->GetRotation()->x, transform->GetRotation()->y, transform->GetRotation()->z));
	}
	
}

GameObject::GameObject(btRigidBody* body){

	this->transform = new Transform();

	this->body = body;
}

GameObject::GameObject(Mesh* mesh){
	this->transform = new Transform();

	// Make a default rigidbody
	btDefaultMotionState* state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	body = new btRigidBody(1, state, new btBoxShape(btVector3(1, 1, 1)), btVector3(transform->GetRotation()->x, transform->GetRotation()->y, transform->GetRotation()->z));

	SetMesh(mesh);
}

GameObject::~GameObject()
{
}

void GameObject::Update(float dt){
	
}

ID3D11Buffer* const* GameObject::getMeshVertexArray(){
	return mMesh->getVertexBuffer();
}

ID3D11Buffer* GameObject::getMeshIndexArray(){
	return mMesh->getIndexBuffer();
}

int GameObject::getMeshIndexCount(){
	return mMesh->getIndexCount();
}

void GameObject::SetMesh(Mesh* mesh){
	this->mMesh = mesh;
}

void GameObject::SetMaterial(Material* material){
	this->mMaterial = material;
}

Mesh* GameObject::GetMesh(){
	return mMesh;
}

Material* GameObject::GetMaterial(){
	return mMaterial;
}

Texture* GameObject::GetTexture(){
	return texture;
}

bool GameObject::IsEnabled(){
	return enabled;
}

void GameObject::Enable(){
	enabled = true;
}

void GameObject::Disable(){
	enabled = false;
}


