#pragma once
#include <Windows.h>
#include <DirectXMath.h>
#include <vector>
#include <d3d11.h>
#include <DDSTextureLoader.h>
#include "DirectXGame.h"
#include "Vertex.h"
#include "SimpleShader.h"
#include "Camera.h"


using namespace DirectX;

class Skybox
{
public:

	Skybox(ID3D11Device*, SimpleVertexShader*, SimplePixelShader*);
	~Skybox();

	ID3D11Buffer* GetSphereIndBuffer();
	ID3D11Buffer* GetSphereVertBuffer();
	SimpleVertexShader* GetSkyMapVertShader();
	SimplePixelShader* GetSkyMapPixelShader();
	ID3D11ShaderResourceView* GetSkyResourceView();
	ID3D11DepthStencilState* GetStencilState();
	ID3D11RasterizerState*	GetRasterizerState();
	int GetNumVerts();
	int GetNumFaces();
	XMFLOAT4X4 GetSphereWorldMatrix(XMFLOAT3 pos);


	void SetSphereIndBuffer();
	void SetSphereVertBuffer();
	void SetSkyMapVertShader();
	void SetSkyMapPixelShader();
	void SetSkyResourceView();
	void SetStencilState();
	void SetRasterizerState();
	void SetNumVerts();
	void SetNumFaces();
	void SetSphereWorldMatrix();

	void CreateSphere(int, int);
	void LoadCubeMap();
	void SetRenderStates();
	void DrawSkybox(ID3D11DeviceContext*, const UINT, const UINT, Camera*);

	XMFLOAT4X4* sphereWorld;

private:
	ID3D11Buffer* sphereIndexBuffer;
	ID3D11Buffer* sphereVertBuffer;

	SimpleVertexShader* SKYMAP_VS;
	SimplePixelShader* SKYMAP_PS;

	ID3D11ShaderResourceView* resourceVeiw;
	ID3D11SamplerState* samplerState;
	ID3D11Resource* textureResource;

	ID3D11DepthStencilState* DSLessEqual;
	ID3D11RasterizerState* RSCullNone;

	int NumSphereVertices;
	int NumSphereFaces;

	ID3D11Device* device;
};