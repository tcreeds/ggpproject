struct VertexToPixel
{
	float4 position		: SV_POSITION;
	float2 uv			: TEXCOORD;
};

Texture2D diffuseTexture : register(t0);
SamplerState basicSampler : register(s0);

float4 main(VertexToPixel input) : SV_TARGET
{

	float4 screenColor = diffuseTexture.Sample(basicSampler, input.uv);

	if (screenColor.x > 0.7)
	{
		return screenColor * 1.3f;
	}
	
	return float4 (0.0f, 0.0f, 0.0f, 1.0f);
}