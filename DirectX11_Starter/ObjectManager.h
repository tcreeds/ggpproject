#pragma once
#include "GameObject.h"
#include "PhysicsManager.h"
#include "Mesh.h"
#include "Player.h"
#include "Camera.h"
#include "GameObjectData.h"
#include "Level.h"
#include "tinyxml2.h"
#include "PowerUp.h"

using namespace tinyxml2;

class ObjectManager
{
public:
	ObjectManager();
	~ObjectManager();
	void init(ID3D11Device* device, ID3D11DeviceContext* deviceContext);
	GameObjectData* GetGameObjectData(const char* name);
	GameObject* CreateGameObject(const char* objectName, XMFLOAT3 position, bool addCollider);
	GameObject* CreateGameObject(Mesh* mesh, Material* material, Texture* texture, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale);
	GameObject* CreateGameObject(Mesh* mesh, Material* material, Texture* texture, btRigidBody* body, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale);
	GameObject* CreatePlatform(const char* objectName, XMFLOAT3 position);
	PowerUp* CreatePowerUp(const char* name, Mesh* mesh, Material* material, Texture* texture, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale);
	//Mesh* CreateMesh(const char* name);
	Player* CreatePlayer(Mesh* mesh, Material* material, Texture* texture, btRigidBody* body, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale);
	int ObjectCount();
	void ParseXML();
	GameObject* gameObjects[50];
	Level* level;

private:
	XMFLOAT3 ParseVec3(std::string str);

private:
	std::vector<GameObjectData*> objectDesc;
	int meshCount;
	int objectCount;
	ID3D11Device* device;
	ID3D11DeviceContext* deviceContext;
	tinyxml2::XMLDocument* doc;
};

