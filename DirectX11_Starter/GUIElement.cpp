#include "GUIElement.h"


GUIElement::GUIElement(float width, float height, float x, float y, Texture* defaultTexture, Texture* selectedTexture, std::string name)
	: width(width), height(height), x(x), y(y), defaultTexture(defaultTexture), selectedTexture(selectedTexture), name(name)
{	
	selected = false;
	pressed = false;
}

GUIElement::GUIElement(float width, float height, float x, float y, Texture* defaultTexture, Texture* selectedTexture, Texture* pressedTexture, std::string name)
	: width(width), height(height), x(x), y(y), defaultTexture(defaultTexture), selectedTexture(selectedTexture), pressedTexture(pressedTexture), name(name)
{
	selected = false;
	pressed = false;
}

GUIElement::GUIElement(float width, float height, float x, float y, Texture* defaultTexture, Texture* selectedTexture, std::string name, std::string action)
	: width(width), height(height), x(x), y(y), defaultTexture(defaultTexture), selectedTexture(selectedTexture), name(name), action(action)
{
	selected = false;
	pressed = false;
}

GUIElement::GUIElement(float width, float height, float x, float y, Texture* defaultTexture, Texture* selectedTexture, Texture* pressedTexture, std::string name, std::string action)
	: width(width), height(height), x(x), y(y), defaultTexture(defaultTexture), selectedTexture(selectedTexture), pressedTexture(pressedTexture), name(name), action(action)
{
	selected = false;
	pressed = false;
}

GUIElement::~GUIElement()
{
}
