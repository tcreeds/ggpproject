#pragma once
#include "Mesh.h"
#include "Material.h"
#include "Texture.h"
#include "Transform.h"
#include "btBulletDynamicsCommon.h"

class GameObject
{
public:
	GameObject();
	GameObject(Mesh* mes, Material* mat, Texture* texture, btRigidBody* bod, XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale);
	GameObject(bool addCollider);
	GameObject(btRigidBody* body);
	GameObject(Mesh* mesh);
	virtual ~GameObject();
	virtual void Update(float dt);
	ID3D11Buffer* const* getMeshVertexArray();
	ID3D11Buffer* getMeshIndexArray();
	int getMeshIndexCount();
	void SetMesh(Mesh* mesh);
	void SetMaterial(Material* material);
	void SetTexture(Texture* texture);
	Mesh* GetMesh();
	Material* GetMaterial();
	Texture* GetTexture();
	Transform* transform;
	btRigidBody* body;
	bool IsEnabled();
	void Enable();
	void Disable();
	XMFLOAT3 graphicsOffset;
	enum GameObjectType{ POWERUP, STATIC, DYNAMIC };
	GameObjectType type;

protected:
	Mesh* mMesh;
	Material* mMaterial;
	Texture* texture;
	//XMFLOAT4X4 transform;
	XMFLOAT3 velocity;
	float time;
	bool enabled;
};

