#pragma once

#include <Windows.h>
#include <DirectXMath.h>
#include <d3d11.h>
#include "DirectXGame.h"
#include "SimpleShader.h"


class RenderTarget
{
public:
	RenderTarget(ID3D11Device*, ID3D11DeviceContext*, float, float, const WCHAR*, const WCHAR*);
	void Set(ID3D11DeviceContext*, ID3D11SamplerState*, ID3D11ShaderResourceView*);
	~RenderTarget();

	//Public Properties
	ID3D11Texture2D* renderTexture;
	ID3D11RenderTargetView* renderTextureView;
	ID3D11ShaderResourceView* renderTextureSRV;

	ID3D11SamplerState* samplerState;
	ID3D11ShaderResourceView* shaderResourceView;
	SimpleVertexShader* vertexShader;
	SimplePixelShader* pixelShader;
};

