#include "Player.h"


Player::Player(Mesh* mesh) : GameObject(mesh)
{
}

Player::Player(Mesh* mesh, Material* material, Texture* texture, btRigidBody* body, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale) : GameObject(mesh, material, texture, body, position, rotation, scale){
	maxJumps = 1;
	jumps = maxJumps;
	jumpForce = 30.0f;
	speed = 0.12f;
	turnSpeed = 0.01f;
	canDie = true;
}

Player::~Player()
{
}

void Player::Update(float dt){
	if (jumpLockTime > 0)
		jumpLockTime -= dt;
}

void Player::Jump(){

	if (jumps > 0){
		jumpLockTime = 0.1f;
		body->applyCentralImpulse(btVector3(0, jumpForce, 0));
		jumps--;
	}
}

void Player::Turn(Direction dir){

	if (dir == LEFT){
		//body->applyTorque(btVector3(0, -turnSpeed, 0));
		//body->applyTorqueImpulse(btVector3(0, -turnSpeed, 0));
		transform->RotateEuler(0, -turnSpeed, 0);
	}
	else if (dir == RIGHT){
		//body->applyTorque(btVector3(0, turnSpeed, 0));
		transform->RotateEuler(0, turnSpeed, 0);
	}
}

void Player::Move(Direction dir){

	XMFLOAT3 vec = XMFLOAT3(0, 0, 0);
	if (dir == FORWARD){
		vec = transform->forward;
	}
	else if (dir == BACK){
		XMStoreFloat3(&vec, XMLoadFloat3(&transform->forward) * -1);
	}
	else if (dir == LEFT){
		XMStoreFloat3(&vec, XMLoadFloat3(&transform->right) * -1);
	}
	else if (dir == RIGHT){
		vec = transform->right;
	}

	body->translate(btVector3(vec.x * speed, vec.y * speed, vec.z * speed));

}

void Player::ResetJump(){
	if (jumpLockTime <= 0){
		jumpLockTime = 0;
		jumps = maxJumps;
	}
}
