#pragma once
#include "Vertex.h"
#include "DirectXGame.h"
#include "WICTextureLoader.h"

using namespace DirectX;

class Texture
{
public:
	Texture(ID3D11Device* device, ID3D11DeviceContext* deviceContext, const WCHAR* filepath, const char* name);
	~Texture();
	ID3D11ShaderResourceView* shaderResourceView;
	ID3D11SamplerState* samplerState;
	const char* name;
};

