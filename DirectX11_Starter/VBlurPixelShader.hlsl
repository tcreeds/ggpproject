Texture2D diffuseTexture : register(t0);
SamplerState basicSampler : register(s0);


// Defines the input to this pixel shader
// - Should match the output of our corresponding vertex shader
struct VertexToPixel
{
	float4 position		: SV_POSITION;
	float2 uv			: TEXCOORD;
};

cbuffer Window : register(b0)
{
	float height;
};



float4 main(VertexToPixel input) : SV_TARGET
{
	float4 screenColor = diffuseTexture.Sample(basicSampler, input.uv);

	screenColor += diffuseTexture.Sample(basicSampler, input.uv + float2(0, (1 / height)) * 2);
	screenColor += diffuseTexture.Sample(basicSampler, input.uv + float2(0, -(1 / height)) * 2);
	screenColor += diffuseTexture.Sample(basicSampler, input.uv + float2(0, (1 / height)) * 3);
	screenColor += diffuseTexture.Sample(basicSampler, input.uv + float2(0, -(1 / height)) * 3);

	return (screenColor / 5);
}