#pragma once
#include "Vertex.h"

class GameObjectData
{
public:
	GameObjectData(const char* name, const char* mesh, const char* material, const char* texture, const char* bodyType, XMFLOAT3 extents);
	~GameObjectData();
	const char* name;
	const char* mesh;
	const char* material;
	const char* texture;
	const char* bodyType;
	XMFLOAT3 extents;
};

