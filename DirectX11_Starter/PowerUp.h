#pragma once
#include "GameObject.h"
class PowerUp :
	public GameObject
{
public:
	PowerUp(const char* name, Mesh* mesh, Material* material, Texture* texture, btRigidBody* body, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale);
	virtual ~PowerUp();
	const char* name;
	void Update(float dt);
	float startHeight;
	float elapsedTime;
};

