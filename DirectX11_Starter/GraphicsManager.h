#pragma once
#include "GameObject.h"
#include "Camera.h"
#include "Mesh.h"
#include "Vertex.h"
#include "WICTextureLoader.h"
#include "Texture.h"
#include "Skybox.h"
#include "RenderTarget.h"
#include "GUIView.h"
#include <fstream>

class GraphicsManager
{
public:
	GraphicsManager(ID3D11Device* device, ID3D11DeviceContext* deviceContext);
	~GraphicsManager();
	void init();
	void Render(GameObject* gameObjects[], int numObjects, Camera* camera);
	void DrawGUI(GUIView* gui);
	void RegisterMesh(Mesh* m);
	void DrawObject(GameObject* obj, Camera* camera);
	void SetRenderTargetView(ID3D11RenderTargetView* rtv);
	void SetDepthStencilView(ID3D11DepthStencilView* dsv);
	void SetClearColor(float color[4]);
	//void MakePointLight(PointLight light);
	Mesh* GetMesh(const char* name);
	Material* GetMaterial(const char* name);
	Texture* GetTexture(const char* name);
	void MoveSun(float dt);
	void SetWindowDimensions(float width, float height);
	void DrawToTexture();
	void AddPointLight(XMFLOAT3 position, XMFLOAT4 color, float range);
	void AddSpotLight(XMFLOAT3 position, XMFLOAT4 color, XMFLOAT3 direction, float intensity);
	void RenderShadowMap(GameObject* gameObjects[], int numObjects, Camera* camera);
	void DrawShadowMap(GameObject* obj, Camera* camera);
	void SetUpShadowMap();

private:
	void LoadOBJ(const char* filepath, const char* meshName);
	void InitPostProcess();
	void CreateScreen();
	Mesh* CreateMesh(const char* name, std::vector<Vertex> vertices, int numVertices, std::vector<UINT> indices, int numIndices);
	Material* CreateMaterial(const char* name, const WCHAR* vsFile, const WCHAR* psFile);
	void CreateSkymapMaterial(const char* name, const WCHAR* vsFile, const WCHAR* psFile);
	Texture* CreateTexture(const WCHAR* filepath, const char* name);
	ID3D11Buffer* createVertexBuffer(std::vector<Vertex> vertices, int numVertices);
	ID3D11Buffer* createIndexBuffer(std::vector<UINT> indices, int numIndices);

private:
	std::vector<Mesh*> meshes;
	std::vector<Material*> materials;
	std::vector<Texture*> textures;
	DirectionalLight dirLight;
	SpotLight spotLights[8];
	PointLight pointLights[8];
	UINT numSpotLights;
	UINT numPointLights;

	ID3D11Device* device;
	ID3D11DeviceContext* deviceContext;

	ID3D11Texture2D* renderTexture;
	ID3D11RenderTargetView* renderTextureView;
	ID3D11ShaderResourceView* renderTextureSRV;
	ID3D11RenderTargetView* renderTargetView;
	ID3D11DepthStencilView* depthStencilView;
	ID3D11DepthStencilState* depthStencilState;

	//Render Targets
	RenderTarget* bloomFirstPassRenderTarget;
	RenderTarget* h_blurRenderTarget;
	RenderTarget* v_blurRenderTarget;
	RenderTarget* bloomRenderTarget;
	ID3D11Texture2D* shadowMap;
	ID3D11DepthStencilView* shadowMapDSV;
	ID3D11ShaderResourceView* shadowMapSRV;
	XMFLOAT4X4 lightView;
	XMFLOAT4X4 lightProj;
	
	Texture* screenTexture;
	SimpleVertexShader* screenVertexShader;
	SimpleVertexShader* shadowVertexShader;
	SimplePixelShader* screenPixelShader;
	float clearColor[4];
	float windowWidth;
	float windowHeight;

	Skybox* sky;
};

