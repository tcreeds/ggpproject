#pragma once
#include <Windows.h>
#include <DirectXMath.h>
#include "Transform.h"

using namespace DirectX;

class Camera
{
public:
	Camera();
	~Camera();
	void Update(float dt);
	XMFLOAT4X4 GetViewMatrix();
	XMFLOAT4X4 GetProjectionMatrix();
	void UpdateProjectionMatrix(float aspectRatio);
	void FollowTarget(Transform* target);
	void Free();
	void Rotate(float x, float y);
	Transform* transform;
	enum CameraState{ FIRSTPERSON, THIRDPERSON, SCRIPTED };

private:
	Transform* target;
	XMFLOAT3 position;
	XMFLOAT3 direction;
	XMFLOAT3 forward;
	XMFLOAT3 up;
	XMFLOAT4X4 viewMatrix;
	XMFLOAT4X4 projectionMatrix;
	float rotationX;
	float rotationY;
	float fieldOfView;
	float nearClipDistance;
	float farClipDistance;
	CameraState cameraState;
	XMFLOAT3 distance;

};


