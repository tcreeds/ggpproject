#pragma once
#include "GUIElement.h"
#include <vector>

class GUIView
{
public:
	enum Keypress { LEFTCLICK, RIGHTCLICK, SPACE, LEFT, RIGHT, UP, DOWN };
	GUIView(std::string name);
	~GUIView();
	void AddElement(GUIElement element);
	void Select(GUIElement* element);
	void Deselect(GUIElement* element);
	void Press(GUIElement* element); 
	void Keyboard(Keypress key);
	GUIElement* GetElementByName(std::string name);

	std::string name;
	GUIElement* selectedElement;
	std::vector<GUIElement> elements;


};

