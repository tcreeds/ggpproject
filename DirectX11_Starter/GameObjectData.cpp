#include "GameObjectData.h"


GameObjectData::GameObjectData(const char* name, const char* mesh, const char* material, const char* texture, const char* bodyType, XMFLOAT3 extents)
	: name(name), mesh(mesh), material(material), texture(texture), bodyType(bodyType), extents(extents)
{
}


GameObjectData::~GameObjectData()
{
}
