#include "ObjectManager.h"


ObjectManager::ObjectManager()
{
}


ObjectManager::~ObjectManager()
{
}

void ObjectManager::init(ID3D11Device* device, ID3D11DeviceContext* deviceContext){
	this->device = device;
	this->deviceContext = deviceContext;
	meshCount = 0;
	objectCount = 0;

	doc = new tinyxml2::XMLDocument();
	doc->LoadFile("../Debug/xml/game2.xml");
	ParseXML();
	/*LoadOBJ("models/cube.obj", "Cube");
	LoadOBJ("models/torus.obj", "Torus");
	LoadOBJ("models/helix.obj", "Helix");
	LoadOBJ("models/sphere.obj", "Sphere");
	LoadOBJ("models/box.obj", "Box");
	LoadOBJ("models/plane.obj", "Plane");
	LoadOBJ("models/Rock1.obj", "Rock1");*/
}

GameObject* ObjectManager::CreateGameObject(Mesh* mesh, Material* material, Texture* texture, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale){
	GameObject* obj = new GameObject(mesh, material, texture, NULL, position, rotation, scale);
	obj->type = GameObject::STATIC;
	gameObjects[objectCount++] = obj;
	return obj;
}

GameObject* ObjectManager::CreateGameObject(Mesh* mesh, Material* material, Texture* texture, btRigidBody* body, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale){
	GameObject* obj = new GameObject(mesh, material, texture, body, position, rotation, scale);
	obj->type = GameObject::DYNAMIC;
	gameObjects[objectCount++] = obj;
	return obj;
}

PowerUp* ObjectManager::CreatePowerUp(const char* name, Mesh* mesh, Material* material, Texture* texture, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale){

	PowerUp* obj = new PowerUp(name, mesh, material, texture, NULL, position, rotation, scale);
	obj->type = GameObject::POWERUP;
	gameObjects[objectCount++] = obj;
	return obj;

}

Player* ObjectManager::CreatePlayer(Mesh* mesh, Material* material, Texture* texture, btRigidBody* body, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale){

	Player* obj = new Player(mesh, material, texture, body, position, rotation, scale);
	obj->type = GameObject::DYNAMIC;
	gameObjects[objectCount++] = obj;
	return obj;
}

int ObjectManager::ObjectCount(){
	return objectCount;
}

GameObjectData* ObjectManager::GetGameObjectData(const char* name){
	for (int i = 0; i < objectDesc.size(); i++){
		if (strcmp(objectDesc[i]->name, name) == 0){
			return objectDesc[i];
		}
	}
	return 0;
}


void ObjectManager::ParseXML(){

	XMLElement* root = doc->FirstChildElement("game");
	XMLElement* currObject = root->FirstChildElement("objects")->FirstChildElement("gameObject");;

	while (currObject != NULL){

		auto name = currObject->FirstChildElement("name");
		auto mesh = currObject->FirstChildElement("mesh");
		auto material = currObject->FirstChildElement("material");
		auto texture = currObject->FirstChildElement("texture");
		auto bodyType = currObject->FirstChildElement("bodyType");
		auto extents = ParseVec3(currObject->FirstChildElement("extents")->GetText());

		GameObjectData* data = new GameObjectData(
			name != NULL ? name->GetText() : NULL,
			mesh != NULL ? mesh->GetText() : NULL,
			material != NULL ? material->GetText() : NULL,
			texture != NULL ? texture->GetText() : NULL,
			bodyType != NULL ? bodyType->GetText() : NULL,
			extents);

		objectDesc.push_back(data);
		currObject = currObject->NextSiblingElement("gameObject");
	}

	XMLElement* levelnode = root->FirstChildElement("level");
	currObject = levelnode->FirstChildElement("object");
	level = new Level();
	while (currObject != NULL){
		auto name = currObject->FirstChildElement("name")->GetText();
		auto pos = ParseVec3(currObject->FirstChildElement("position")->GetText());
		auto rot = ParseVec3(currObject->FirstChildElement("rotation")->GetText());
		//auto pos = XMFLOAT3(atof(currObject->FirstChildElement("positionX")->GetText()), atof(currObject->FirstChildElement("positionY")->GetText()), atof(currObject->FirstChildElement("positionZ")->GetText()));
		//auto rot = XMFLOAT3(atof(currObject->FirstChildElement("rotationX")->GetText()), atof(currObject->FirstChildElement("rotationY")->GetText()), atof(currObject->FirstChildElement("rotationZ")->GetText()));
		int sc = atof(currObject->FirstChildElement("scale")->GetText());
		float mass = atof(currObject->FirstChildElement("mass")->GetText());
		auto scale = XMFLOAT3(sc, sc, sc);

		level->objects.push_back(new Level::ObjectData(name, pos, rot, scale, mass));
		currObject = currObject->NextSiblingElement("object");
	}

}

XMFLOAT3 ObjectManager::ParseVec3(std::string str){
	XMFLOAT3 vec;
	int delim = str.find(',');
	std::string x = str.substr(0, delim);
	str = str.substr(delim+1, str.length() - delim - 1);
	delim = str.find(',');
	std::string y = str.substr(0, delim);
	std::string z = str.substr(delim+1, str.length() - delim-1);
	vec.x = stof(x);
	vec.y = stof(y);
	vec.z = stof(z);

	return vec;
}