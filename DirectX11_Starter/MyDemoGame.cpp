// ----------------------------------------------------------------------------
//  A few notes on project settings
//
//  - The project is set to use the UNICODE character set
//    - This was changed in Project Properties > Config Properties > General > Character Set
//    - This basically adds a "#define UNICODE" to the project
//
//  - The include directories were automagically correct, since the DirectX 
//    headers and libs are part of the windows SDK
//    - For instance, $(WindowsSDK_IncludePath) is set as a project include 
//      path by default.  That's where the DirectX headers are located.
//
//  - Two libraries had to be manually added to the Linker Input Dependencies
//    - d3d11.lib
//    - d3dcompiler.lib
//    - This was changed in Project Properties > Config Properties > Linker > Input > Additional Dependencies
//
//  - The Working Directory was changed to match the actual .exe's 
//    output directory, since we need to load the compiled shader files at run time
//    - This was changed in Project Properties > Config Properties > Debugging > Working Directory
//
// ----------------------------------------------------------------------------

#include "MyDemoGame.h"

#pragma region Win32 Entry Point (WinMain)

//using namespace DirectX;

// Win32 Entry Point
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	// Make the game, initialize and run
	MyDemoGame game(hInstance);
	
	// If we can't initialize, we can't run
	if( !game.Init() )
		return 0;
	
	// All set to run the game
	return game.Run();
}

#pragma endregion

#pragma region Constructor / Destructor

MyDemoGame::MyDemoGame(HINSTANCE hInstance) : DirectXGame(hInstance)
{
	// Set up our custom caption and window size
	windowCaption = L"Demo DX11 Game";
	windowWidth = 800;
	windowHeight = 600;
}

MyDemoGame::~MyDemoGame()
{
	delete triangle;
	delete square;
	delete parallelogram;
	delete mainCamera;
	for (int i = 0; i < entityCount; i++)
		delete entities[i];

	// Release all of the D3D stuff that's still hanging out
	ReleaseMacro(vertexShader);
	ReleaseMacro(pixelShader);
	ReleaseMacro(vsConstantBuffer);
	ReleaseMacro(inputLayout);
}

#pragma endregion

#pragma region Initialization

// Initializes the base class (including the window and D3D),
// sets up our geometry and loads the shaders (among other things)
bool MyDemoGame::Init()
{
	// Make sure DirectX initializes properly
	if( !DirectXGame::Init() )
		return false;


	gameState = MAINMENU;
	objectManager = new ObjectManager();
	objectManager->init(device, deviceContext);
	graphicsManager = new GraphicsManager(device, deviceContext);
	graphicsManager->SetWindowDimensions(windowWidth, windowHeight);
	graphicsManager->init();
	graphicsManager->SetRenderTargetView(renderTargetView);
	graphicsManager->SetDepthStencilView(depthStencilView);

	gui = new GUI(graphicsManager);

	// Successfully initialized
	return true;
}

void MyDemoGame::StartGame(){

	//deviceContext->OMGetDepthStencilState(&depthStencilState, 0);
	gui->currentView = NULL;
	gameState = GAME;
	gamePad = std::move(std::unique_ptr<GamePad>(new GamePad));
	spriteBatch = std::move(std::unique_ptr<SpriteBatch>(new SpriteBatch(deviceContext)));
	spriteFont = std::move(std::unique_ptr<SpriteFont>(new SpriteFont(device, L"../Debug/font.spritefont")));

	physicsManager = new PhysicsManager();

	
	// Load pixel & vertex shaders, and then create an input layout

	//LoadShadersAndInputLayout();

	LoadLevel(objectManager->level);

	mainCamera = new Camera();
	mainCamera->FollowTarget(player->transform);
	mainCamera->UpdateProjectionMatrix(AspectRatio());
	
	light1.AmbientColor = XMFLOAT4(0.2f, 0.2f, 0.2f, 1);
	light1.DiffuseColor = XMFLOAT4(0.7f, 0.7f, 0.7f, 1);
	light1.Direction = XMFLOAT3(0, -1, 0);

	/*light2.AmbientColor = XMFLOAT4(0.2f, 0.0f, 0.0f, 1);
	light2.DiffuseColor = XMFLOAT4(0.5f, 0.5f, 0.5f, 1);
	light2.Direction = XMFLOAT3(-1, 0, 0);
	*/

	//move texture creation into objectManager or graphicsManager, store list and set sampler of material as needed
	CreateWICTextureFromFile(device, deviceContext, L"textures/wood.jpg", 0, &textureSRV);

	// Make our "ground"
	physicsManager->MakeGround();

	// Make/register the physics bodies for the objects
	//physicsManager->RegisterBody(player);
	for (int i = 0; i < objectManager->ObjectCount(); i++)
	{
		objectManager->gameObjects[i]->transform->Translate(0, -2, 0);
		if (objectManager->gameObjects[i]->body != NULL){
			physicsManager->RegisterBody(objectManager->gameObjects[i]);
		}
	}

}

#pragma endregion

#pragma region Window Resizing

// Handles resizing the window and updating our projection matrix to match
void MyDemoGame::OnResize()
{
	// Handle base-level DX resize stuff
	DirectXGame::OnResize();
	if (mainCamera)
		mainCamera->UpdateProjectionMatrix(AspectRatio());
	if (graphicsManager)
		graphicsManager->SetWindowDimensions(windowWidth, windowHeight);
}
#pragma endregion

#pragma region Game Loop

// Update your game state
void MyDemoGame::UpdateScene(float dt)
{

	if (gameState == GAME){
		mainCamera->Update(dt);
		for (int i = 0; i < objectManager->ObjectCount(); i++)
			objectManager->gameObjects[i]->Update(dt);


		auto state = gamePad->GetState(0);

		//check gamepad input
		if (state.IsConnected()){

			float moveX = state.thumbSticks.leftX * 0.01f;
			float moveY = state.thumbSticks.leftY * 0.01f;
			float turnX = state.thumbSticks.rightX * 0.001f;
			float turnY = state.thumbSticks.rightY * 0.001f;
			player->transform->MoveForward(moveY);
			player->transform->Strafe(moveX);
			player->transform->RotateEuler(0, turnX, 0);
		}

		//check keyboard input
		if (GetAsyncKeyState(VK_LEFT) & 0x8000){
			player->Turn(Player::LEFT);
		}
		else if (GetAsyncKeyState(VK_RIGHT) & 0x8000){
			player->Turn(Player::RIGHT);
		}
		if (GetAsyncKeyState(0x57) & 0x8000){
			player->Move(Player::FORWARD);
			player->transform->RotateEuler(0.1, 0, 0);
		}
		if (GetAsyncKeyState(0x41) & 0x8000){
			player->Move(Player::LEFT);
		}
		if (GetAsyncKeyState(0x53) & 0x8000){
			player->Move(Player::BACK);
			player->transform->RotateEuler(-0.1, 0, 0);
		}
		if (GetAsyncKeyState(0x44) & 0x8000){
			player->Move(Player::RIGHT);
		}

		//checking player for collision to determine jump status
		int numManifolds = physicsManager->GetWorld()->getDispatcher()->getNumManifolds();
		for (int i = 0; i < numManifolds; i++){

			btPersistentManifold* contactManifold = physicsManager->GetWorld()->getDispatcher()->getManifoldByIndexInternal(i);
			btCollisionObject* objA = const_cast<btCollisionObject* > (contactManifold->getBody0());
			btCollisionObject* objB = const_cast<btCollisionObject* > (contactManifold->getBody1());

			if (player->body == objA || player->body == objB){
				int numContacts = contactManifold->getNumContacts();
				for (int j = 0; j < numContacts; j++){
					btManifoldPoint& point = contactManifold->getContactPoint(j);
					float dist = point.getDistance();
					if (dist <= 0.0f){
						float angle = GGPMath::dot(&btVector3(0, 1, 0), &point.m_normalWorldOnB);
						if (player->body == objA){
							if (angle > 0)
								player->ResetJump();
						}
						else {
							if (angle < 0)
								player->ResetJump();
						}
					}
				}
			}
		}
		
		if (GetAsyncKeyState(0x43) & 0x8000 != cameraButtonDown){
			gameState = DEB;
			mainCamera->Free();
		}

		physicsManager->Update(dt);

		for (int i = 0; i < objectManager->ObjectCount(); i++)
		{
			if (objectManager->gameObjects[i]->body != NULL)
				physicsManager->SyncWithGraphic(objectManager->gameObjects[i]);
			if (objectManager->gameObjects[i]->type == GameObject::POWERUP){
				if (objectManager->gameObjects[i]->IsEnabled()){
					objectManager->gameObjects[i]->Update(dt);
					XMFLOAT3 playerPos = *player->transform->GetPosition();
					XMFLOAT3 powerPos = *objectManager->gameObjects[i]->transform->GetPosition();
					if (abs(playerPos.x - powerPos.x) < 2 && abs(playerPos.y - powerPos.y) < 2 && abs(playerPos.z - powerPos.z) < 2){
						objectManager->gameObjects[i]->Disable();
						//apply double jump
						if (strcmp(static_cast<PowerUp*>(objectManager->gameObjects[i])->name, "DoubleJump") == 0){
							player->maxJumps = 2;
							player->jumps = player->maxJumps;
						}
						else if (strcmp(static_cast<PowerUp*>(objectManager->gameObjects[i])->name, "LavaResist") == 0){
							player->canDie = false;
						}
						else if (strcmp(static_cast<PowerUp*>(objectManager->gameObjects[i])->name, "End") == 0){
							exit(0);
						}
					}
				}
			}

			XMFLOAT3 pP = *player->transform->GetPosition();
		

			if (player->canDie == true){
				if (pP.z > 100.0)
				{
					btVector3 pos = player->body->getCenterOfMassPosition();
					player->body->translate(-pos);
				}
			}
		}

		if ((GetAsyncKeyState(VK_SPACE) & 0x8000) && player->jumps > 0 && !spacePressed)
		{
			player->Jump();
		}
	}
	else if (gameState == DEB){
		mainCamera->Update(dt);
		if (GetAsyncKeyState(VK_LEFT) & 0x8000){
			mainCamera->Rotate(0, -0.01f);
		}
		if (GetAsyncKeyState(VK_RIGHT) & 0x8000){
			mainCamera->Rotate(0, 0.01f);
		}
		if (GetAsyncKeyState(0x57) & 0x8000){
			mainCamera->transform->MoveForward(0.9f);
		}
		if (GetAsyncKeyState(0x41) & 0x8000){
			mainCamera->transform->Strafe(-0.9f);
		}
		if (GetAsyncKeyState(0x53) & 0x8000){
			mainCamera->transform->MoveForward(-0.9f);
		}
		if (GetAsyncKeyState(0x44) & 0x8000){
			mainCamera->transform->Strafe(0.9f);
		}
		if (GetAsyncKeyState(0x43) & 0x8000 != cameraButtonDown){
			gameState = GAME;
			mainCamera->FollowTarget(player->transform);
		}

	}
	else if (gameState == MAINMENU){
		if (GetAsyncKeyState(VK_RETURN) & 0x8000){
			StartGame();
		}
	}
	cameraButtonDown = GetAsyncKeyState(0x43) & 0x8000;
	spacePressed = GetAsyncKeyState(VK_SPACE) & 0x8000;
	
}

void MyDemoGame::LoadLevel(Level* level){

	auto objectsInScene = level->objects;

	physicsManager->CreateAndRegisterBody(XMFLOAT3(0,0,0), XMFLOAT3(5, 20, 5), XMFLOAT3(2,10,10), XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), 0);

	player = objectManager->CreatePlayer(
		graphicsManager->GetMesh("Ball"),		//mesh
		graphicsManager->GetMaterial("Default"),		//material
		graphicsManager->GetTexture("Wood"),
		physicsManager->CreateBody(XMFLOAT3(0, 5, 0), "Sphere", XMFLOAT3(1, 2, 1), XMFLOAT3(0, 0, 0), XMFLOAT3(1, 1, 1), 1),	//physics body
		XMFLOAT3(0, 5, 0),						//position
		XMFLOAT3(0, 0, 0),
		XMFLOAT3(1.3, 1.3, 1.3));

	objectManager->CreateGameObject(
		graphicsManager->GetMesh("Ground"),
		graphicsManager->GetMaterial("Default"),
		graphicsManager->GetTexture("Ground"),
		XMFLOAT3(0, 3, 0),
		XMFLOAT3(0, 0, 0),
		XMFLOAT3(1, 1, 1));

	objectManager->CreateGameObject(
		graphicsManager->GetMesh("Mountain"),
		graphicsManager->GetMaterial("Default"),
		graphicsManager->GetTexture("Ground"),
		XMFLOAT3(4, 0, 55),
		XMFLOAT3(0, 0, 0),
		XMFLOAT3(1, 1, 1));
	
	objectManager->CreateGameObject(
		graphicsManager->GetMesh("Ground2"),
		graphicsManager->GetMaterial("Default"),
		graphicsManager->GetTexture("Ground"),
		XMFLOAT3(0, 3, 199.8),
		XMFLOAT3(0, 0, 0),
		XMFLOAT3(1, 1, 1));

	objectManager->CreateGameObject(
		graphicsManager->GetMesh("Lava"),
		graphicsManager->GetMaterial("Default"),
		graphicsManager->GetTexture("LavaTex"),
		XMFLOAT3(0, 3, 133.75),
		XMFLOAT3(0, 0, 0),
		XMFLOAT3(1, 1, 1));

	for (int i = 0; i < objectsInScene.size(); i++){

		auto objectInfo = objectsInScene[i];
		auto data = objectManager->GetGameObjectData(objectInfo->name);
		if (strcmp(data->name, "Power") == 0){
			objectManager->CreatePowerUp(
				"DoubleJump",
				graphicsManager->GetMesh(data->mesh),
				graphicsManager->GetMaterial(data->material),
				graphicsManager->GetTexture(data->texture),
				objectInfo->position,
				objectInfo->rotation,
				objectInfo->scale);
		}
		else if (strcmp(data->name, "Power2") == 0){
			objectManager->CreatePowerUp(
				"LavaResist",
				graphicsManager->GetMesh(data->mesh),
				graphicsManager->GetMaterial(data->material),
				graphicsManager->GetTexture(data->texture),
				objectInfo->position,
				objectInfo->rotation,
				objectInfo->scale);
		}
		else if (strcmp(data->name, "Power3") == 0){
			objectManager->CreatePowerUp(
				"End",
				graphicsManager->GetMesh(data->mesh),
				graphicsManager->GetMaterial(data->material),
				graphicsManager->GetTexture(data->texture),
				objectInfo->position,
				objectInfo->rotation,
				objectInfo->scale);
		}
		else{
			objectManager->CreateGameObject(
				graphicsManager->GetMesh(data->mesh),
				graphicsManager->GetMaterial(data->material),
				graphicsManager->GetTexture(data->texture),
				physicsManager->CreateBody(objectInfo->position, data->bodyType, data->extents, objectInfo->rotation, objectInfo->scale, objectInfo->mass),
				objectInfo->position,
				objectInfo->rotation,
				objectInfo->scale);
		}


	}
}

// Clear the screen, redraw everything, present
void MyDemoGame::DrawScene()
{
	// Background color (Cornflower Blue in this case) for clearing
	float color[4] = {0.4f, 0.6f, 0.75f, 0.0f};
	const float debugColor[4] = { 0.1f, 0.4f, 0.1f, 0.0f };
	if (gameState == GAME || gameState == DEB){
		physicsManager->SyncWithGraphic(player);
		graphicsManager->Render(objectManager->gameObjects, objectManager->ObjectCount(), mainCamera);
		

		std::wstring str = std::to_wstring(prevMousePos.x) + L"  " + std::to_wstring(prevMousePos.y);
		spriteBatch->Begin();
		spriteFont->DrawString(spriteBatch.get(), str.data(), XMFLOAT2(10, 10));
		spriteBatch->End();
		

		
	}
	else if (gameState == MAINMENU){
		graphicsManager->DrawGUI(gui->currentView);
	}
	HR(swapChain->Present(0, 0));
}

#pragma endregion

#pragma region Mouse Input

// These methods don't do much currently, but can be used for mouse-related input

void MyDemoGame::OnMouseDown(WPARAM btnState, int x, int y)
{
	mouseDown = true;
	prevMousePos.x = x;
	prevMousePos.y = y;
	std::string action = gui->MouseDown();
	if (action == "startgame"){
		StartGame();
	}
	SetCapture(hMainWnd);
}

void MyDemoGame::OnMouseUp(WPARAM btnState, int x, int y)
{
	mouseDown = false;
	ReleaseCapture();
}

void MyDemoGame::OnMouseMove(WPARAM btnState, int x, int y)
{

	if (mouseDown && mainCamera != NULL){
		/*float dy = (y - prevMousePos.y) / 70.0f;
		float dx = (x - prevMousePos.x) / 100.0f;
		
		entities[0]->Rotate(0, dx, 0);*/
		mainCamera->Rotate((y - prevMousePos.y) / 70.0f, (x - prevMousePos.x) / 70.0f);
	}
		
	float gl_x = (x - windowWidth / 2.0f) / (windowWidth / 2.0f);
	float gl_y = (y - windowHeight / 2.0f) / (windowHeight / 2.0f) * -1.0f;

	gui->MouseMove(gl_x, gl_y);

	prevMousePos.x = x;
	prevMousePos.y = y;
}
#pragma endregion

void MyDemoGame::ChangeState(std::string newState){
	if (newState == "game"){
		gameState = GAME;
	}
	else if (newState == "debug"){
		gameState = DEB;
	}
	else if (newState == "mainmenu"){
		gameState = MAINMENU;
	}
}