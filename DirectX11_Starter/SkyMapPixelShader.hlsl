TextureCube SkyMap : register(t0);
SamplerState basicSampler : register(s0);

struct SKYMAP_VS_OUTPUT	//output structure for skymap vertex shader
{
	float4 Pos : SV_POSITION;
	float3 texCoord : TEXCOORD;
};

float4 main(SKYMAP_VS_OUTPUT input) : SV_Target
{
	return SkyMap.Sample(basicSampler, input.texCoord);
}