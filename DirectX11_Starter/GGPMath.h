#pragma once
#include "Vertex.h"
#include "btBulletDynamicsCommon.h"

class GGPMath
{

public:
	GGPMath();
	~GGPMath();
	static float dot(XMFLOAT3 a, XMFLOAT3 b);
	static float dot(btVector3* a, btVector3* b);

};

