#include "Camera.h"
#include <iostream>


Camera::Camera()
{
	position = XMFLOAT3(0, 0, -10);
	forward = XMFLOAT3(0, 0, 1);
	up = XMFLOAT3(0, 1, 0);
	DirectX::XMStoreFloat4x4(&viewMatrix, XMMatrixIdentity());
	rotationX = 0.0f;
	rotationY = 0.0f;
	distance = XMFLOAT3(0, 5, -20);
	cameraState = FIRSTPERSON;
	fieldOfView = 0.25f;
	nearClipDistance = 0.1f;
	farClipDistance = 500.0f;
	transform = new Transform();
	
}


Camera::~Camera()
{
}

void Camera::Update(float dt){

	if (cameraState == THIRDPERSON){
		float rotation = target->GetRotation()->y;
		XMFLOAT3* pos = target->GetPosition();
		//position = XMFLOAT3(pos->x + distance.x, pos->y + distance.y, pos->z + distance.z);
		position = XMFLOAT3(pos->x + sin(rotation) * distance.z, pos->y + distance.y, pos->z + cos(rotation) * distance.z);
		XMMATRIX trans = XMMatrixLookAtLH(XMLoadFloat3(&position), XMLoadFloat3(target->GetPosition()), XMLoadFloat3(&up));
		DirectX::XMStoreFloat4x4(&viewMatrix, XMMatrixTranspose(trans));
		XMVECTOR rot;
		XMVECTOR translation;
		XMVECTOR scale;
		XMMatrixDecompose(&scale, &rot, &translation, XMMatrixTranspose(trans));
		XMFLOAT3 r;
		XMStoreFloat3(&r, rot);
		transform->SetRotation(r.x, r.y, 0);
		transform->SetPosition(position);

	}
	else if (cameraState == FIRSTPERSON){

		XMMATRIX mat = XMMatrixRotationRollPitchYaw(transform->GetRotation()->x, transform->GetRotation()->y, transform->GetRotation()->z);
		XMVECTOR dir = XMVector3Transform(XMLoadFloat3(&forward), mat);
		DirectX::XMStoreFloat3(&direction, dir);
		XMMATRIX trans = XMMatrixLookToLH(XMLoadFloat3(transform->GetPosition()), dir, XMLoadFloat3(&up));
		DirectX::XMStoreFloat4x4(&viewMatrix, XMMatrixTranspose(trans));

	}

	

	
	
}

XMFLOAT4X4 Camera::GetViewMatrix(){
	return viewMatrix;
}

XMFLOAT4X4 Camera::GetProjectionMatrix(){
	return projectionMatrix;
}

void Camera::UpdateProjectionMatrix(float aspectRatio){

	XMMATRIX P = XMMatrixPerspectiveFovLH(
		fieldOfView * 3.1415926535f,		// Field of View Angle
		aspectRatio,				// Aspect ratio
		nearClipDistance,						// Near clip plane distance
		farClipDistance);					// Far clip plane distance
	XMStoreFloat4x4(&projectionMatrix, XMMatrixTranspose(P));

}

void Camera::Rotate(float x, float y){

	transform->RotateEuler(x / 5, y / 5, 0);

}

void Camera::FollowTarget(Transform* target){
	this->cameraState = THIRDPERSON;
	this->target = target;


	float rotation = target->GetRotation()->y;
	XMFLOAT3* pos = target->GetPosition();
	//position = XMFLOAT3(pos->x + distance.x, pos->y + distance.y, pos->z + distance.z);
	position = XMFLOAT3(pos->x + sin(rotation) * distance.z, pos->y + distance.y, pos->z + cos(rotation) * distance.z);
	XMMATRIX trans = XMMatrixLookAtLH(XMLoadFloat3(&position), XMLoadFloat3(target->GetPosition()), XMLoadFloat3(&up));
	DirectX::XMStoreFloat4x4(&viewMatrix, XMMatrixTranspose(trans));
}

void Camera::Free(){
	this->cameraState = FIRSTPERSON;
}

