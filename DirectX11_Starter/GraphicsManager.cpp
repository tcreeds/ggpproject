#include "GraphicsManager.h"


GraphicsManager::GraphicsManager(ID3D11Device* device, ID3D11DeviceContext* deviceContext)
{
	this->device = device;
	this->deviceContext = deviceContext;
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	dsDesc.StencilEnable = false;
	dsDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	dsDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	device->CreateDepthStencilState(&dsDesc, &depthStencilState);
	numSpotLights = 0;
	numPointLights = 0;
}


GraphicsManager::~GraphicsManager()
{
}

void GraphicsManager::init(){

	LoadOBJ("../Debug/models/cube.obj", "Cube");
	//LoadOBJ("models/torus.obj", "Torus");
	//LoadOBJ("models/helix.obj", "Helix");
	//LoadOBJ("models/sphere.obj", "Sphere");
	LoadOBJ("../Debug/models/Ball.obj", "Ball");
	LoadOBJ("../Debug/models/box.obj", "Box");
	//LoadOBJ("models/plane.obj", "Plane");
	LoadOBJ("../Debug/models/ground.obj", "Ground");
	LoadOBJ("../Debug/models/Pine.obj", "Pine");
	LoadOBJ("../Debug/models/Stump.obj", "Stump");
	LoadOBJ("../Debug/models/Power.obj", "Power");
	LoadOBJ("../Debug/models/Rock_3.obj", "Rock");
	LoadOBJ("../Debug/models/Lava.obj", "Lava");
	LoadOBJ("../Debug/models/Ground2.obj", "Ground2");	//load materials
	LoadOBJ("../Debug/models/Mountains.obj", "Mountain");
	LoadOBJ("../Debug/models/Plat.obj", "Platform");
	CreateScreen();

	CreateTexture(L"../Debug/textures/wood.jpg", "Wood");
	CreateTexture(L"../Debug/textures/ground.jpg", "Ground");
	CreateTexture(L"../Debug/textures/smiley.jpg", "Smiley");
	CreateTexture(L"../Debug/textures/lava.png", "LavaTex");
	CreateTexture(L"../Debug/textures/start.png", "Start");
	CreateTexture(L"../Debug/textures/start hover.png", "StartHover");
	CreateTexture(L"../Debug/textures/stump.jpg", "Stumptxt");
	CreateTexture(L"../Debug/textures/rock.jpg", "Rocktxt");

	CreateSkymapMaterial("Skymap", L"../Debug/SkyMapVertexShader.cso", L"../Debug/SkyMapPixelShader.cso");

	CreateMaterial("Default", L"../Debug/VertexShader.cso", L"../Debug/PixelShader.cso");
	CreateMaterial("GUI", L"../Debug/GUIVertexShader.cso", L"../Debug/GUIPixelShader.cso");

	screenTexture = new Texture(device, deviceContext, 0, "Screen");
	screenVertexShader = new SimpleVertexShader(device, deviceContext);
	screenVertexShader->LoadShaderFile(L"../Debug/ScreenVertexShader.cso");
	screenPixelShader = new SimplePixelShader(device, deviceContext);
	screenPixelShader->LoadShaderFile(L"../Debug/ScreenPixelShader.cso");
	shadowVertexShader = new SimpleVertexShader(device, deviceContext);
	shadowVertexShader->LoadShaderFile(L"../Debug/ShadowVertexShader.cso");


	bloomFirstPassRenderTarget = new RenderTarget(device, deviceContext, windowWidth, windowHeight, L"../Debug/VBlurVertexShader.cso", L"../Debug/BloomFirstPassPixelShader.cso");
	h_blurRenderTarget = new RenderTarget(device, deviceContext, windowWidth, windowHeight, L"../Debug/VBlurVertexShader.cso", L"../Debug/HBlurPixelShader.cso");
	v_blurRenderTarget = new RenderTarget(device, deviceContext, windowWidth, windowHeight, L"../Debug/VBlurVertexShader.cso", L"../Debug/VBlurPixelShader.cso");
	bloomRenderTarget = new RenderTarget(device, deviceContext, windowWidth, windowHeight, L"../Debug/VBlurVertexShader.cso", L"../Debug/BloomPixelShader.cso");
	
	
	InitPostProcess();
	
	//lights
	dirLight.AmbientColor = XMFLOAT4(0.05f, 0.05f, 0.05f, 1);
	dirLight.DiffuseColor = XMFLOAT4(0.7f, 0.7f, 0.7f, 1);
	dirLight.Direction = XMFLOAT3(0, -1, 0);
	
	AddPointLight(XMFLOAT3(80, 40, 15), XMFLOAT4(0.5f, 0.0f, 0.5f, 1), 20.0f);
	AddPointLight(XMFLOAT3(-25, 60, 40), XMFLOAT4(0.5f, 0.0f, 0.5f, 1), 20.0f);
	AddPointLight(XMFLOAT3(78, 5, 210), XMFLOAT4(0.5f, 0.5f, 0.5f, 1), 20.0f);
	AddPointLight(XMFLOAT3(0.0f, 0.0f, 40.0f), XMFLOAT4(-0.1f, -0.1f, -0.1f, 1.0f), 20.0f);
	//AddPointLight(XMFLOAT3(20.0f, 0.0f, 0.0f), XMFLOAT4(0.5f, 0.0f, 0.0f, 1.0f), 20.0f);
	//AddPointLight(XMFLOAT3(-20.0f, 0.0f, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.5f, 1.0f), 20.0f);


	SetUpShadowMap();
}

void GraphicsManager::SetUpShadowMap()
{
	D3D11_TEXTURE2D_DESC texDesc;
	ZeroMemory(&texDesc, sizeof(D3D11_TEXTURE2D_DESC));
	texDesc.Width = windowWidth;
	texDesc.Height = windowHeight;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;//DXGI_FORMAT_R32_TYPELESS;
	texDesc.SampleDesc.Count = 1;
	texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;


	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	srvDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS; //DXGI_FORMAT_R32_FLOAT;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = texDesc.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	ZeroMemory(&dsvDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	dsvDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT; //DXGI_FORMAT_D32_FLOAT;
	dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Texture2D.MipSlice = 0;

	HR(device->CreateTexture2D(&texDesc, 0, &shadowMap));
	HR(device->CreateDepthStencilView(shadowMap, &dsvDesc, &shadowMapDSV));
	HR(device->CreateShaderResourceView(shadowMap, &srvDesc, &shadowMapSRV));

	XMFLOAT3 pos = XMFLOAT3(0, 300, 0); //XMFLOAT3(0, 100, 0);
	XMMATRIX trans = XMMatrixLookAtLH(XMLoadFloat3(&pos), XMLoadFloat3(&XMFLOAT3(0, 0, 0)), XMLoadFloat3(&XMFLOAT3(-1, 0, 0)));
	XMStoreFloat4x4(&lightView, XMMatrixTranspose(trans));
	XMMATRIX P = XMMatrixPerspectiveFovLH(
		0.5 * 3.1415926535f,		// Field of View Angle
		(float)windowWidth / windowHeight,				// Aspect ratio
		0.1f,						// Near clip plane distance
		500.0f);					// Far clip plane distance
	XMStoreFloat4x4(&lightProj, XMMatrixTranspose(P));
}

void GraphicsManager::RenderShadowMap(GameObject* gameObjects[], int numObjects, Camera* camera)
{


	deviceContext->PSSetShader(0, 0, 0); // Unbind pixel shader since we don't want it
	deviceContext->OMSetRenderTargets(0, 0, shadowMapDSV);
	deviceContext->ClearDepthStencilView(shadowMapDSV, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);


	//draw world objects
	for (int i = 0; i < numObjects; i++){
		if (gameObjects[i]->IsEnabled()){
			DrawShadowMap(gameObjects[i], camera);
		}
	}
}

void GraphicsManager::Render(GameObject* gameObjects[], int numObjects, Camera* camera){


	deviceContext->OMSetDepthStencilState(depthStencilState, 0);

	RenderShadowMap(gameObjects, numObjects, camera);

	const float color[4] = { 0.4f, 0.6f, 0.75f, 0.0f };

	//prepare first render target
	deviceContext->OMSetRenderTargets(1, &renderTextureView, depthStencilView);
	deviceContext->ClearRenderTargetView(renderTextureView, color);
	deviceContext->ClearDepthStencilView(
		depthStencilView,
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.0f,
		0); 

	//set parameters to draw other objects
	deviceContext->OMSetDepthStencilState(depthStencilState, 0);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//draw skybox
	UINT offset = 0;
	UINT stride = sizeof(Vertex);
	sky->DrawSkybox(deviceContext, stride, 0, camera);

	//draw world objects
	for (int i = 0; i < numObjects; i++){
		if (gameObjects[i]->IsEnabled()){
			DrawObject(gameObjects[i], camera);			
		}
	}

	Mesh* screenQuad = GetMesh("screenQuad");

	//set parameters to draw other objects
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//Bloom Shaders
	bloomFirstPassRenderTarget->Set(deviceContext, screenTexture->samplerState, screenTexture->shaderResourceView);
	DrawToTexture();

	h_blurRenderTarget->Set(deviceContext, bloomFirstPassRenderTarget->samplerState, bloomFirstPassRenderTarget->shaderResourceView);
	h_blurRenderTarget->pixelShader->SetFloat("width", windowWidth);
	DrawToTexture();

	v_blurRenderTarget->Set(deviceContext, h_blurRenderTarget->samplerState, h_blurRenderTarget->shaderResourceView);
	v_blurRenderTarget->pixelShader->SetFloat("height", windowHeight);
	DrawToTexture();

	bloomRenderTarget->Set(deviceContext, screenTexture->samplerState, screenTexture->shaderResourceView);
	bloomRenderTarget->pixelShader->SetShaderResourceView("brightTexture", v_blurRenderTarget->shaderResourceView);
	DrawToTexture();

	
	//Render Targets
	//set render target to back buffer
	deviceContext->OMSetRenderTargets(1, &renderTargetView, 0);
	deviceContext->ClearRenderTargetView(renderTargetView, color);
	//deviceContext->OMSetDepthStencilState(depthStencilState, 0);

	//set screen space shaders
	screenVertexShader->SetShader();
	screenPixelShader->SetSamplerState("basicSampler", bloomRenderTarget->samplerState);
	screenPixelShader->SetShaderResourceView("diffuseTexture", bloomRenderTarget->shaderResourceView);
	screenPixelShader->SetShader();

	deviceContext->IASetVertexBuffers(0, 1, screenQuad->getVertexBuffer(), &stride, &offset);
	deviceContext->IASetIndexBuffer(screenQuad->getIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

	// Draw
	deviceContext->DrawIndexed(screenQuad->getIndexCount(), 0, 0);

	// Unset the shader resource
	ID3D11ShaderResourceView* unset[1] = { 0 };
	deviceContext->PSSetShaderResources(0, 1, unset);
}

void GraphicsManager::DrawShadowMap(GameObject* obj, Camera* camera)
{
	Material* material = obj->GetMaterial();
	Mesh* mesh = obj->GetMesh();
	Texture* texture = obj->GetTexture();
	shadowVertexShader->SetMatrix4x4("world", *(obj->transform->GetMatrix()));
	shadowVertexShader->SetMatrix4x4("lightView", lightView);
	shadowVertexShader->SetMatrix4x4("lightProj", lightProj);

	shadowVertexShader->SetShader();



	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	deviceContext->IASetVertexBuffers(0, 1, mesh->getVertexBuffer(), &stride, &offset);
	deviceContext->IASetIndexBuffer(mesh->getIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

	deviceContext->DrawIndexed(
		mesh->getIndexCount(),	// The number of indices we're using in this draw
		0,
		0);

}

void GraphicsManager::DrawObject(GameObject* obj, Camera* camera){

	Material* material = obj->GetMaterial();
	Mesh* mesh = obj->GetMesh();
	Texture* texture = obj->GetTexture();
	//btScalar matrix[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } ;
	//obj->body->getWorldTransform().getOpenGLMatrix(matrix);
	material->getVertexShader()->SetMatrix4x4("world", *(obj->transform->GetMatrix()));
	//material->getVertexShader()->SetMatrix4x4("world", matrix);
	material->getVertexShader()->SetMatrix4x4("view", camera->GetViewMatrix());
	material->getVertexShader()->SetMatrix4x4("projection", camera->GetProjectionMatrix());
	material->getVertexShader()->SetMatrix4x4("lightView", lightView);
	material->getVertexShader()->SetMatrix4x4("lightProj", lightProj);

	material->getPixelShader()->SetSamplerState("basicSampler", texture->samplerState);
	material->getPixelShader()->SetShaderResourceView("diffuseTexture", texture->shaderResourceView);
	material->getPixelShader()->SetShaderResourceView("shadowMap", shadowMapSRV);

	//take care of lights
	material->getPixelShader()->SetData(
		"dirLight",
		&dirLight,
		sizeof(DirectionalLight));
	float size = sizeof(PointLight)* 8;
	material->getPixelShader()->SetData(
		"pointLights",
		&pointLights,
		size);
	material->getPixelShader()->SetData(
		"spotLights",
		&spotLights[0],
		sizeof(SpotLight) * 8);
	material->getPixelShader()->SetFloat("numPointLights", numPointLights);
	material->getPixelShader()->SetFloat("numSpotLights", numSpotLights);

	material->getVertexShader()->SetShader();
	material->getPixelShader()->SetShader();

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	deviceContext->IASetVertexBuffers(0, 1, mesh->getVertexBuffer(), &stride, &offset);
	deviceContext->IASetIndexBuffer(mesh->getIndexBuffer() , DXGI_FORMAT_R32_UINT, 0);

	deviceContext->DrawIndexed(
		mesh->getIndexCount(),	// The number of indices we're using in this draw
		0,
		0);

	// Unbind all SRV's from PS stage
	ID3D11ShaderResourceView* srvUnset[16] = { 0 };
	deviceContext->PSSetShaderResources(0, 16, srvUnset);
}

void GraphicsManager::DrawGUI(GUIView* view){
	Material* mat = GetMaterial("GUI");
	const float color[4] = { 0.1f, 0.4f, 0.1f, 0.0f };
	//set render target to back buffer
	deviceContext->OMSetRenderTargets(1, &renderTargetView, 0);
	deviceContext->ClearRenderTargetView(renderTargetView, color);
	deviceContext->ClearDepthStencilView(
		depthStencilView,
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.0f,
		0);
	deviceContext->OMSetDepthStencilState(depthStencilState, 0);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	for (int i = 0; i < view->elements.size(); i++){
		
		GUIElement el = view->elements[i];
		XMFLOAT4X4 matrix;
		mat->getVertexShader()->SetFloat2("scale", XMFLOAT2(el.width/2.0f, el.height/2.0f));
		mat->getVertexShader()->SetFloat2("offset", XMFLOAT2(el.x, el.y));
		if (el.selected){
			mat->getPixelShader()->SetSamplerState("basicSampler", el.selectedTexture->samplerState);
			mat->getPixelShader()->SetShaderResourceView("diffuseTexture", el.selectedTexture->shaderResourceView);
		}
		else{
			mat->getPixelShader()->SetSamplerState("basicSampler", el.defaultTexture->samplerState);
			mat->getPixelShader()->SetShaderResourceView("diffuseTexture", el.defaultTexture->shaderResourceView);
		}
		mat->getPixelShader()->SetShader();
		mat->getVertexShader()->SetShader();

		Mesh* screenQuad = GetMesh("screenQuad");
		UINT stride = sizeof(Vertex);
		UINT offset = 0;
		deviceContext->IASetVertexBuffers(0, 1, screenQuad->getVertexBuffer(), &stride, &offset);
		deviceContext->IASetIndexBuffer(screenQuad->getIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

		// Draw
		deviceContext->DrawIndexed(screenQuad->getIndexCount(), 0, 0);
	}
}

void GraphicsManager::InitPostProcess(){
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS;

	device->CreateSamplerState(&samplerDesc, &screenTexture->samplerState);

	// Set up the render texture
	D3D11_TEXTURE2D_DESC rtDesc;
	rtDesc.Width = windowWidth;
	rtDesc.Height = windowHeight;
	rtDesc.ArraySize = 1;
	rtDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	rtDesc.CPUAccessFlags = 0;
	rtDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	rtDesc.MipLevels = 1;
	rtDesc.MiscFlags = 0;
	rtDesc.SampleDesc.Count = 1;
	rtDesc.SampleDesc.Quality = 0;
	rtDesc.Usage = D3D11_USAGE_DEFAULT;
	device->CreateTexture2D(&rtDesc, 0, &renderTexture);

	// Set up the render target view
	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	ZeroMemory(&rtvDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	rtvDesc.Format = rtDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvDesc.Texture2D.MipSlice = 0;
	device->CreateRenderTargetView(renderTexture, &rtvDesc, &renderTextureView);

	// Set up a shader resource view
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	srvDesc.Format = rtDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.MostDetailedMip = 0;
	device->CreateShaderResourceView(renderTexture, &srvDesc, &screenTexture->shaderResourceView);
}

void GraphicsManager::LoadOBJ(const char* filepath, const char* meshName){

	// File input object
	std::ifstream obj(filepath); // <-- Replace filename with your parameter

	// Check for successful open
	if (!obj.is_open())
		return;

	// Variables used while reading the file
	std::vector<XMFLOAT3> positions;     // Positions from the file
	std::vector<XMFLOAT3> normals;       // Normals from the file
	std::vector<XMFLOAT2> uvs;           // UVs from the file
	std::vector<Vertex> verts;           // Verts we're assembling
	std::vector<UINT> indices;           // Indices of these verts
	unsigned int triangleCounter = 0;    // Count of triangles/indices
	char chars[100];                     // String for line reading

	// Still good?
	while (obj.good())
	{
		// Get the line (100 characters should be more than enough)
		obj.getline(chars, 100);

		// Check the type of line
		if (chars[0] == 'v' && chars[1] == 'n')
		{
			// Read the 3 numbers directly into an XMFLOAT3
			XMFLOAT3 norm;
			sscanf_s(
				chars,
				"vn %f %f %f",
				&norm.x, &norm.y, &norm.z);

			// Add to the list of normals
			normals.push_back(norm);
		}
		else if (chars[0] == 'v' && chars[1] == 't')
		{
			// Read the 2 numbers directly into an XMFLOAT2
			XMFLOAT2 uv;
			sscanf_s(
				chars,
				"vt %f %f",
				&uv.x, &uv.y);

			// Add to the list of uv's
			uvs.push_back(uv);
		}
		else if (chars[0] == 'v')
		{
			// Read the 3 numbers directly into an XMFLOAT3
			XMFLOAT3 pos;
			sscanf_s(
				chars,
				"v %f %f %f",
				&pos.x, &pos.y, &pos.z);

			// Add to the positions
			positions.push_back(pos);
		}
		else if (chars[0] == 'f')
		{
			// Read the 9 face indices into an array
			unsigned int i[9];
			sscanf_s(
				chars,
				"f %d/%d/%d %d/%d/%d %d/%d/%d",
				&i[0], &i[1], &i[2],
				&i[3], &i[4], &i[5],
				&i[6], &i[7], &i[8]);

			// - Create the verts by looking up
			//    corresponding data from vectors
			// - OBJ File indices are 1-based, so
			//    they need to be adusted
			Vertex v1;
			v1.Position = positions[i[0] - 1];
			v1.UV = uvs[i[1] - 1];
			v1.Normal = normals[i[2] - 1];

			Vertex v2;
			v2.Position = positions[i[3] - 1];
			v2.UV = uvs[i[4] - 1];
			v2.Normal = normals[i[5] - 1];

			Vertex v3;
			v3.Position = positions[i[6] - 1];
			v3.UV = uvs[i[7] - 1];
			v3.Normal = normals[i[8] - 1];

			// Add the verts to the vector
			verts.push_back(v1);
			verts.push_back(v2);
			verts.push_back(v3);

			// Add three more indices
			indices.push_back(triangleCounter++);
			indices.push_back(triangleCounter++);
			indices.push_back(triangleCounter++);
		}
	}

	obj.close();

	CreateMesh(meshName, verts, triangleCounter, indices, triangleCounter);
}

void GraphicsManager::SetWindowDimensions(float width, float height){
	windowWidth = width;
	windowHeight = height;
}

void GraphicsManager::AddPointLight(XMFLOAT3 position, XMFLOAT4 color, float range){
	pointLights[numPointLights] = PointLight();
	pointLights[numPointLights].Position = XMFLOAT4(position.x, position.y, position.z, 1.0f);
	pointLights[numPointLights].Color = color;
	pointLights[numPointLights].Range = range;
	numPointLights++;
}

void GraphicsManager::AddSpotLight(XMFLOAT3 position, XMFLOAT4 color, XMFLOAT3 direction, float intensity){
	spotLights[numSpotLights] = SpotLight();
	spotLights[numSpotLights].Position = XMFLOAT4(position.x, position.y, position.z, 1.0f);
	spotLights[numSpotLights].Color = color;
	spotLights[numSpotLights].Direction = XMFLOAT4(direction.x, direction.y, direction.z, 1.0f);
	numSpotLights++;

}

Mesh* GraphicsManager::CreateMesh(const char* name, std::vector<Vertex> vertices, int numVertices, std::vector<UINT> indices, int numIndices){

	Mesh* m = new Mesh(name);
	m->SetData(vertices, numVertices, indices, numIndices);
	ID3D11Buffer* vb = createVertexBuffer(m->getVertices(), numVertices);
	ID3D11Buffer* ib = createIndexBuffer(indices, numIndices);
	m->SetBuffers(vb, ib);
	meshes.push_back(m);
	return m;
}

void GraphicsManager::CreateScreen(){

	std::vector<XMFLOAT2> uvs;           // UVs from the file
	std::vector<Vertex> verts;           // Verts we're assembling
	std::vector<UINT> indices;

	Vertex v1;
	v1.Position = XMFLOAT3(-1, +1, 0);
	v1.UV = XMFLOAT2(0, 0);
	Vertex v2;
	v2.Position = XMFLOAT3(-1, -1, 0);
	v2.UV = XMFLOAT2(0, 1);
	Vertex v3;
	v3.Position = XMFLOAT3(+1, -1, 0);
	v3.UV = XMFLOAT2(1, 1);
	Vertex v4;
	v4.Position = XMFLOAT3(+1, +1, 0);
	v4.UV = XMFLOAT2(1, 0);

	verts.push_back(v1);
	verts.push_back(v2);
	verts.push_back(v3);
	verts.push_back(v4);

	indices.push_back(0);
	indices.push_back(2);
	indices.push_back(1);
	indices.push_back(0);
	indices.push_back(3);
	indices.push_back(2);

	Mesh* m = new Mesh("screenQuad");
	m->SetData(verts, 4, indices, 6);
	ID3D11Buffer* vb = createVertexBuffer(m->getVertices(), 4);
	ID3D11Buffer* ib = createIndexBuffer(indices, 6);
	m->SetBuffers(vb, ib);
	meshes.push_back(m);
}

Material* GraphicsManager::CreateMaterial(const char* name, const WCHAR* vsFile, const WCHAR* psFile){
	SimpleVertexShader* vs = new SimpleVertexShader(device, deviceContext);
	SimplePixelShader* ps = new SimplePixelShader(device, deviceContext);
	vs->LoadShaderFile(vsFile);
	ps->LoadShaderFile(psFile);
	Material* m = new Material(name, ps, vs);
	materials.push_back(m);
	return m;
}

void GraphicsManager::CreateSkymapMaterial(const char* name, const WCHAR* vsFile, const WCHAR* psFile)
{
	SimpleVertexShader* vs = new SimpleVertexShader(device, deviceContext);
	SimplePixelShader* ps = new SimplePixelShader(device, deviceContext);
	vs->LoadShaderFile(vsFile);
	ps->LoadShaderFile(psFile);
	sky = new Skybox(device, vs, ps);
}

Texture* GraphicsManager::CreateTexture(const WCHAR* filepath, const char* name){
	auto texture = new Texture(device, deviceContext, filepath, name);
	textures.push_back(texture);
	return texture;
}

Mesh* GraphicsManager::GetMesh(const char* name){
	for (int i = 0; i < meshes.size(); i++){
		if (strcmp(meshes[i]->GetName(), name) == 0){
			return meshes[i];
		}
	}
	return 0;
}

Material* GraphicsManager::GetMaterial(const char* name){
	for (int i = 0; i < materials.size(); i++){
		if (strcmp(materials[i]->GetName(), name) == 0){
			return materials[i];
		}
	}
	return 0;
}

Texture* GraphicsManager::GetTexture(const char* name){
	for (int i = 0; i < textures.size(); i++){
		if (strcmp(textures[i]->name, name) == 0){
			return textures[i];
		}
	}
	return 0;
}

ID3D11Buffer* GraphicsManager::createVertexBuffer(std::vector<Vertex> vertices, int numVertices){

	ID3D11Buffer* vb;

	// Create the vertex buffer
	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(Vertex)* numVertices; // Number of vertices in the "model" you want to draw
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA initialVertexData;
	initialVertexData.pSysMem = vertices.data();
	HR(device->CreateBuffer(&vbd, &initialVertexData, &vb));

	return vb;
}

ID3D11Buffer* GraphicsManager::createIndexBuffer(std::vector<UINT> indices, int numIndices){

	ID3D11Buffer* ib;
	// Create the index buffer
	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(UINT)* numIndices; // Number of indices in the "model" you want to draw
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	ibd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA initialIndexData;
	initialIndexData.pSysMem = indices.data();
	HR(device->CreateBuffer(&ibd, &initialIndexData, &ib));

	return ib;
}

void GraphicsManager::MoveSun(float time){
	dirLight.Direction.x = sin(time);
	dirLight.Direction.y = cos(time);
}

void GraphicsManager::SetRenderTargetView(ID3D11RenderTargetView* rtv){
	renderTargetView = rtv;
}

void GraphicsManager::SetDepthStencilView(ID3D11DepthStencilView* dsv){
	depthStencilView = dsv;
}

void GraphicsManager::SetClearColor(float color[4]){
	for (int i = 0; i < 4; i++)
		clearColor[i] = color[i];
}

/*void GraphicsManager::MakePointLight(PointLight light)
{
	int length = sizeof(pointLights) / sizeof(*pointLights);
	if (length < 10)
	{
		pointLights[length] = light;
		numPointLights++;
	}

}*/

void GraphicsManager::DrawToTexture()
{
	Mesh* screenQuad = GetMesh("screenQuad");
	UINT offset = 0;
	UINT stride = sizeof(Vertex);
	deviceContext->IASetVertexBuffers(0, 1, screenQuad->getVertexBuffer(), &stride, &offset);
	deviceContext->IASetIndexBuffer(screenQuad->getIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

	// Draw
	deviceContext->DrawIndexed(screenQuad->getIndexCount(), 0, 0);
}

