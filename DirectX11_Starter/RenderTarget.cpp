#include "RenderTarget.h"


RenderTarget::RenderTarget(ID3D11Device* device, ID3D11DeviceContext* deviceContext, float windowWidth, float windowHeight, const WCHAR* vertexShaderPath, const WCHAR* pixelShaderPath)
{
	vertexShader = new SimpleVertexShader(device, deviceContext);
	vertexShader->LoadShaderFile(vertexShaderPath);
	pixelShader = new SimplePixelShader(device, deviceContext);
	pixelShader->LoadShaderFile(pixelShaderPath);

	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS;

	device->CreateSamplerState(&samplerDesc, &samplerState);

	// Set up the render texture
	D3D11_TEXTURE2D_DESC rtDesc;
	rtDesc.Width = windowWidth;
	rtDesc.Height = windowHeight;
	rtDesc.ArraySize = 1;
	rtDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	rtDesc.CPUAccessFlags = 0;
	rtDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	rtDesc.MipLevels = 1;
	rtDesc.MiscFlags = 0;
	rtDesc.SampleDesc.Count = 1;
	rtDesc.SampleDesc.Quality = 0;
	rtDesc.Usage = D3D11_USAGE_DEFAULT;
	device->CreateTexture2D(&rtDesc, 0, &renderTexture);

	// Set up the render target view
	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	ZeroMemory(&rtvDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	rtvDesc.Format = rtDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvDesc.Texture2D.MipSlice = 0;
	device->CreateRenderTargetView(renderTexture, &rtvDesc, &renderTextureView);

	// Set up a shader resource view
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	srvDesc.Format = rtDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.MostDetailedMip = 0;
	device->CreateShaderResourceView(renderTexture, &srvDesc, &shaderResourceView);
}

void RenderTarget::Set(ID3D11DeviceContext* deviceContext, ID3D11SamplerState* in_ss, ID3D11ShaderResourceView* in_srv)
{
	const float color[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	deviceContext->OMSetRenderTargets(1, &renderTextureView, 0);
	deviceContext->ClearRenderTargetView(renderTextureView, color);

	vertexShader->SetShader();
	pixelShader->SetSamplerState("basicSampler", in_ss);
	pixelShader->SetShaderResourceView("diffuseTexture", in_srv);
	pixelShader->SetShader();
}

RenderTarget::~RenderTarget()
{

}


