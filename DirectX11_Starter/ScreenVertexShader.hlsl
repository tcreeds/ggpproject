
struct VertexShaderInput
{
	float3 position		: POSITION;
	float3 normal		: NORMAL;
	float2 uv			: TEXCOORD;
};

struct VertexToPixel
{
	float4 position		: SV_POSITION;
	float2 uv			: TEXCOORD;
};

// The entry point for our vertex shader
VertexToPixel main(VertexShaderInput input)
{
	VertexToPixel output;

	output.position = float4(input.position, 1);
	output.uv = input.uv;

	return output;
}