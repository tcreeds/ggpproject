#include "GUI.h"


GUI::GUI(GraphicsManager* gm)
{
	GUIView mainMenu = GUIView("Main Menu");
	mainMenu.AddElement(GUIElement(0.5f, 0.3f, 0.0f, 0.0f, gm->GetTexture("Start"), gm->GetTexture("StartHover"), "Start", "startgame"));
	views.push_back(mainMenu);
	currentView = &views[0];
	currentView->Select(currentView->GetElementByName("Start"));
}

GUI::~GUI()
{
}

void GUI::MouseMove(float x, float y){
	if (currentView != NULL){
		for (int i = 0; i < currentView->elements.size(); i++){
			GUIElement el = currentView->elements[i];
			if (x > el.x - el.width / 2 && x < el.x + el.width / 2 && y > el.y - el.height / 2 && y < el.y + el.height / 2){
				currentView->Select(&currentView->elements[i]);
			}
			else{
				currentView->Deselect(&currentView->elements[i]);
			}
		}
	}

}

void GUI::MouseUp(){

}

std::string GUI::MouseDown(){
	if (currentView != NULL){
		if (currentView->selectedElement != 0){
			return currentView->selectedElement->action;
		}
	}
	return "";
}
