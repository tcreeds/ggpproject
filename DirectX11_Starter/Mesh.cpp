#include "Mesh.h"
#include <fstream>
#include <vector>

Mesh::Mesh()
{

}

Mesh::Mesh(const char* inName) : name(inName){
	enabled = true;
}

Mesh::~Mesh()
{
	ReleaseMacro(this->vertexBuffer);
	ReleaseMacro(this->indexBuffer);
}

void Mesh::SetData(std::vector<Vertex> vertices, int numVertices, std::vector<UINT> indices, int numIndices){
	this->vertices = vertices;
	this->numVertices = numVertices;
	this->indices = indices;
	this->numIndices = numIndices;
}

void Mesh::SetBuffers(ID3D11Buffer* vb, ID3D11Buffer* ib){
	this->vertexBuffer = vb;
	this->indexBuffer = ib;
}

ID3D11Buffer* const* Mesh::getVertexBuffer(){
	return &vertexBuffer;
}

ID3D11Buffer* Mesh::getIndexBuffer(){
	return indexBuffer;
}

std::vector<Vertex> Mesh::getVertices(){
	return vertices;
}

std::vector<UINT> Mesh::getIndices(){
	return indices;
}

int Mesh::getIndexCount(){
	return numIndices;
}

const char* Mesh::GetName(){
	return name;
}

bool Mesh::IsEnabled(){
	return enabled;
}

void Mesh::Enable(){
	enabled = true;
}

void Mesh::Disable(){
	enabled = false;
}

