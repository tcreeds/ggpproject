#include "PowerUp.h"


PowerUp::PowerUp(const char* name, Mesh* mesh, Material* material, Texture* texture, btRigidBody* body, XMFLOAT3 position, XMFLOAT3 rotation, XMFLOAT3 scale) : GameObject(mesh, material, texture, body, position, rotation, scale)
{
	this->name = name;
	startHeight = position.y;
	enabled = true;
	elapsedTime = 0;
}


PowerUp::~PowerUp()
{
}

void PowerUp::Update(float dt){
	elapsedTime += dt;
	XMFLOAT3 pos = *transform->GetPosition();
	pos.y = startHeight + sin(elapsedTime);
	transform->SetPosition(pos);
	transform->RotateEuler(0, 0.005f, 0);
}
