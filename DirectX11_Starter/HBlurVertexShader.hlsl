struct VertexShaderInput
{
	float3 position		: POSITION;
	float3 normal		: NORMAL;
	float2 uv			: TEXCOORD;
};

struct VertexToPixel
{
	float4 position		: SV_POSITION;	// System Value Position - Has specific meaning to the pipeline!
	float2 uv			: TEXCOORD;
};

VertexToPixel main(VertexShaderInput input) 
{
	VertexToPixel output;
	output.position = float4(input.position, 1);
	output.uv = input.uv;

	return output;
}