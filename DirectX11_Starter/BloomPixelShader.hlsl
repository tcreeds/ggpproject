struct VertexToPixel
{
	float4 position		: SV_POSITION;
	float2 uv			: TEXCOORD;
};

Texture2D diffuseTexture : register(t0);
Texture2D brightTexture  : register(t1);
SamplerState basicSampler : register(s0);

float4 main(VertexToPixel input) : SV_TARGET
{

	float4 screenColor = diffuseTexture.Sample(basicSampler, input.uv);
	float4 brightColor = brightTexture.Sample(basicSampler, input.uv);


	return screenColor + brightColor;

}