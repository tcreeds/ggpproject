#include "Texture.h"


Texture::Texture(ID3D11Device* device, ID3D11DeviceContext* deviceContext, const WCHAR* filepath, const char* name) : name(name)
{

	CreateWICTextureFromFile(device, deviceContext, filepath, 0, &shaderResourceView);

	D3D11_SAMPLER_DESC sdesc;
	ZeroMemory(&sdesc, sizeof(D3D11_SAMPLER_DESC));
	sdesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sdesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sdesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sdesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sdesc.MaxLOD = 1000;
	device->CreateSamplerState(&sdesc, &samplerState);
}


Texture::~Texture()
{
}
